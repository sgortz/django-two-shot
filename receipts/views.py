# from django.shortcuts import render
from django.views.generic import ListView
from .models import Receipt


class ReceiptListView(ListView):
    model = Receipt
    template_name = "receipts/list.html"
